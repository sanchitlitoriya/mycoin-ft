// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.1;

import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract MyCoin is ERC721Upgradeable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenID;

    address _owner;
    // function init() public initializer() {
    //     _owner = msg.sender;
    // }
    constructor(){
         _owner = msg.sender;
    }

    function mintMyCoin(address _to) public returns(uint256 id) {
        require(msg.sender == _owner,"MyCoin: only owner can mint");
        _tokenID.increment();
        uint256 newTokenId = _tokenID.current();
        _safeMint(_to,newTokenId);
        return newTokenId;
    }

    function burnMyCoin(uint256 tokenID_) public {
        require(msg.sender == ownerOf(tokenID_),"MyCoin: only owner can burn");
        //require ownerOf tokenID should me msg.sender
        _burn(tokenID_);   
    }
    function transferMyCoin(address _to, uint256 tokenID_) public{
        safeTransferFrom(msg.sender, _to, tokenID_);
    }
    function transferMyCoinFrom(address _from, address _to, uint256 _tokenId ) public{
        //approve krna padega phle
        safeTransferFrom(_from, _to, _tokenId);
    }
}